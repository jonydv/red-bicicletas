var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(error, result){
        res.status(200).json({
            bicicletas: result
         });
    });
    
};

exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta({
        code: req.body.code, 
        color: req.body.color, 
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]});

    Bicicleta.add(bici, function (error, newBici){
        res.status(200).json({
            bicicletas: newBici
        });
    });
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(req.body.code, function(err){
    res.status(204).send();
});
};
/*
exports.bicicleta_update = function(req, res){
    var bici = Bicicleta.findByCode(req.params.code, function(err){
        res.status(204).send();
    });
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    

    res.status(200).json({
        bicicleta: bici
    });
}*/