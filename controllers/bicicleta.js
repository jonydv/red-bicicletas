var Bicicleta = require('../models/bicicleta');

/*exports.bicicleta_list = function(req, res){
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}*/

exports.bicicleta_list =  function (req,res) {
    Bicicleta.allBicis(function (error, result) {
        res.render('bicicletas/index', {bicicletas: result});
    });
}


exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    var bici = new Bicicleta({
    code:req.body.code, color:req.body.color, modelo:req.body.modelo,
    ubicacion:  [req.body.lat, req.body.lng]});
    Bicicleta.add(bici, function (error, newBici){
        res.redirect('/bicicletas');
    });

}

/*exports.bicicleta_update_get = function(req, res){
    var bici = Bicicleta.findById(req.params.code);
    
    res.render('bicicletas/update', {bici});
}

exports.bicicleta_update_post = function(req, res){
    var bici = Bicicleta.findById(req.params.code);
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    

    res.redirect('/bicicletas');
}*/

exports.bicicleta_update_get = function (req, res) {
   Bicicleta.findByCode(req.params.code, function(err,bici){
       console.log(req.params);
       console.log(bici);
        res.render('bicicletas/update', {bici});
    }); 
}

exports.bicicleta_update_post = function (req, res) { 
    Bicicleta.findByCode(req.params.code, function (err, bicicleta) {
        bicicleta.code = req.body.code;
        bicicleta.color = req.body.color;
        bicicleta.modelo = req.body.modelo;
        bicicleta.ubicacion = [req.body.lat, req.body.lng];
        bicicleta.save();
        
        res.redirect('/bicicletas');
    });
    
}



exports.bicicleta_delete_post = function(req, res){
    Bicicleta.removeByCode(req.body.code, function(err){
        res.redirect('/bicicletas');
    });

}