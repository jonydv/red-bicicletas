var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + ' | color: ' + this.color
};



bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb);
};

bicicletaSchema.statics.updateInstance = function (code, newCode, color, modelo, ubicacion) {  
    this.findOne({ code: code }, function (err, doc){
        doc.code = newCode;
        doc.color = color;
        doc.modelo = modelo;
        doc.ubicacion = ubicacion;
        doc.save();
    });
};

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb){
    return this.deleteOne({code: aCode}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*

var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + " | color: " + this.color
;}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error('No existe una bicicleta con el id ' + aBiciId);
}

Bicicleta.removeById = function(aBiciId){
    
    for(var i=0; i < Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);//Splice Elimina el elemento de la lista
            break;
        }
    }
}

var a = new Bicicleta(1, 'rojo', 'urbana', [-34.518622, -58.755405]);
var b = new Bicicleta(2, 'blanca', 'playera', [-34.510573, -58.766807]);
var c = new Bicicleta(3, 'verde', 'mountainbike', [-34.507558, -58.764651]);
var d = new Bicicleta(4, 'violeta', 'carreras', [-34.496321, -58.786011]);
var e = new Bicicleta(5, 'azul', 'standard', [-34.487722, -58.772684]);
var f = new Bicicleta(6, 'rosa', 'urbana', [-34.480907, -58.788941]);
var g = new Bicicleta(7, 'celeste', 'playera', [-34.473407, -58.804691]);
var h = new Bicicleta(8, 'negra', 'urbana', [-34.540306, -58.715043]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);
Bicicleta.add(e);
Bicicleta.add(f);
Bicicleta.add(g);
Bicicleta.add(h);

module.exports = Bicicleta;

*/