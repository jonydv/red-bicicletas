require('newrelic');
require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');

const MongoDbStore = require('connect-mongodb-session')(session);

const usuario = require('./models/usuario');
const token = require('./models/token');


var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var authApiRouter = require('./routes/api/auth');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasApiRouter = require('./routes/api/bicicletas');
var usuariosApiRouter = require('./routes/api/usuarios');

let store;
if (process.env.NODE_ENV === 'development'){
    store = new session.MemoryStore;
}else{
  store = new MongoDbStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}


var app = express();

app.set('secretKey', 'jwt_pwd_!!223344');

app.use(session({
  cookie: {maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis'
}));

var mongoose = require('mongoose');
const { assert } = require('console');
//Si estoy en el ambiente de desarrollo usar local host
//sino usar la variable mongo de abajo
var mongoDb = process.env.MONGO_URI;
//mongoose.set('useUnifiedTopology', true);

//mongoose.set('useFindAndModify', false);
mongoose.Promise = global.Promise;

mongoose.connect(mongoDb, {useNewUrlParser: true, useUnifiedTopology: true});
//mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function(req, res){
  res.render('session/login');
});

app.post('/login', function(req, res, next){
  passport.authenticate('local', function(err, usuario, info){
    if (err) return next(err);
    if(!usuario) return res.render('session/login', {info});
    req.login(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

/*app.get(('/forgotPassword'), function(req, res){

});*/

app.get('/forgotPassword', function(req, res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function(req, res){
  usuario.findOne({ email: req.body.email}, function(err, user){
    console.log(user);
    if (!user) return res.render('session/forgotPassword', {info: {message: 'No existe el email para un usuario existente'}});

    user.resetPassword(function(err){
      if (err) return next(err);
      console.log('session/forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage');
  });
});

/*app.get('/resetPassword/:token', function(req, res, next){
  token.findOne({token: req.params.token}, function(err, token){
    if (!token) return res.status(400).send({type: 'not-verified', msg: 'No existe un usuario asociado al token. Verifique que su token no haya expirado.'});

    usuario.findById(token._userId, function(err, usuario){
      if (!usuario) return res.status(400).send({ msg: 'No existe un usuario asociado al token.'});
      res.render('session/resetPassword', {errors: {}, usuario: usuario});
    });
  });
});*/

app.get('/resetPassword/:token', function(req, res, next) {
  console.log(req.params.token);
  token.findOne({ token: req.params.token }, function(err, token) {
    if(!token) return res.status(400).send({ msg: 'No existe un usuario asociado al token, verifique que su token no haya expirado' });
    usuario.findById(token._userId, function(err, user) {
      if(!user) return res.status(400).send({ msg: 'No existe un usuario asociado al token.' });
      res.render('session/resetPassword', {errors: { }, user: user});
    });
  });
});



/*app.post('/resetPassword', function(req, res){
  if (req.body.password != req.body.confirm_password) {
    res.render('session/resetPassword', {errors: {confirm_password: {message: 'No coincide con el password ingresado'}},
  usuario: new usuario({email: req.body.email})});
  return;
}
usuario.findOne({ email: req.body.email}, function(err, usuario){
  usuario.password = req.body.password;
  usuario.save(function(err){
    if (err){
      res.render('session/resetPassword', {errors: err.errors, usuario: new usuario({email: req.body.email})});
    }else{
      res.redirect('/login');
    }
  });
});
});*/

app.post('/resetPassword', function(req, res) {
  if(req.body.password != req.body.confirm_password) {
    res.render('sessions/resetPassword', {errors: {confirm_password: {message: 'No coincide con el password ingresado'}}, usuario: new Usuario({email: req.body.email})});
    return;
  }
  usuario.findOne({email: req.body.email}, function(err, user) {
    user.password = req.body.password;
    user.save(function(err) {
      if(err) {
        res.render('sessions/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})});
      } else {
        res.redirect('/login');
      }
    });
  });
});


function loggedIn(req, res, next){
  if (req.user) {
    next();
  }else {
    console.log('user sin loguearse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if (err){
      console.log('Error al validar el usuario');
      res.json({status:"error", message: err.message, data: null});
    }else{
      req.body.userId = decoded.id;

      console.log('jwt verify: ' + decoded);
      next();
    }
  });
};

app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
//app.use('/bicicletas', bicicletasRouter);
app.use('/api/bicicletas',validarUsuario ,bicicletasApiRouter);
app.use('/api/usuarios', usuariosApiRouter);
app.use('/api/auth', authApiRouter);


app.use('/google6c5c609dcd51f8de', function(req, res){
  res.sendFile('public/google6c5c609dcd51f8de.html');
});

/*app.get('/auth/google',
  passport.authenticate('google', { scope:
      [ 'https://www.googleapis.com/auth/plus.login', 
      'https://www.googleapis.com/auth/plus.profile.emails.read' ] }
));*/

app.get('/auth/google',
  passport.authenticate('google', { scope:
      [ 'https://www.googleapis.com/auth/plus.login', 
      'https://www.googleapis.com/auth/plus.profile.emails.read',
        'profile', 'email', ] }
));

app.get( '/auth/google/callback',
    passport.authenticate( 'google', {
        successRedirect: '/',
        failureRedirect: '/error'
}));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
