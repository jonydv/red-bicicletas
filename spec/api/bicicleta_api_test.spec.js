var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');




var base_url = 'http://localhost:5000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach(function(done) {
        //var mongoDb = 'mongodb://localhost/testdb';
       // mongoose.connect(mongoDb, {useNewUrlParser: true});
        var mongoDb = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDb, {useNewUrlParser: true, useUnifiedTopology: true});
        
       const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database!');
            done();
            
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
            mongoose.disconnect();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('status 200', (done) => {
            
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                console.log(result.bicicletas.length);
                console.log(response.statusCode);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });

        });
    });
    
    describe('POST BICICLETAS /create', () => {
        it('status 200', (done) => {
            var headers = {'content-type': 'application/json' };
            var aBici = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat":-34.518622, "lng":-58.755405}' ;
                
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                
                var bici = JSON.parse(body).bicicletas;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34.518622);
                expect(bici.ubicacion[1]).toBe(-58.755405);
                done();
            });
            
            });
        });
});
   /*
        describe('POST BICICLETAS /create', () => {
            it('status 200', (done) => {
                var headers = {'content-type': 'application/json' };
                var aBici = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat":-34.518622, "lng": -58.755405}';
                request.post({
                    headers: headers,
                    url: base_url + '/create',
                    body: aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                    console.log(bici); 
                    expect(bici.color).toBe("rojo");
                    expect(bici.ubicacion[0]).toBe(-34.518622);
                    expect(bici.ubicacion[1]).toBe(-58.755405);
                    done();
                });
                
                });
            });

        describe("DELETE BICICLETAS /delete", () => {
            it("Status 204", (done) => {
                var a = Bicicleta.createInstance(1, 'negro', 'urbana', [-34.518622, -58.755405]);
                Bicicleta.addListener(a, function(err, newBici){
                    var headers = {'content-type': 'application/json'};
                    url: base_url +'/delete';
                    Bicicleta.removeById(req.body.id);
                    done();
                });
            });
        });
        
});

*/